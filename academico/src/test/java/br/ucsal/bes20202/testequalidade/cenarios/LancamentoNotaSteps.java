package br.ucsal.bes20202.testequalidade.cenarios;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import br.ucsal.bes20202.testequalidade.academico.Aluno;

public class LancamentoNotaSteps {

	private Aluno aluno;

	@Given("um aluno está matriculado na disciplina")
	public void instanciarAluno() {
		// Comportamento de teste
		aluno = new Aluno(); // ele cria um array de notas
	}

	@When("informo a nota $nota")
	public void informarNota(Double nota) {
		// Comportamento de teste
		aluno.informarNota(nota);

	}

	@Then("a situação do aluno é $situacao")
	public void verificarSituacaoAluno(String situacaoEsperada) {
		// Verificação com Assert
		Assert.assertEquals(situacaoEsperada, aluno.obterSituacao());

	}

}
